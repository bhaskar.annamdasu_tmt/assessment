package OopsAbstraction;

public  class Trainee implements DataProvider {
    int marksSecured;
    int totalMaximumMarks;

    void Trainees(int marksSecured,int totalMaximumMarks) {
        this.marksSecured = marksSecured;
        this.totalMaximumMarks=totalMaximumMarks;

    }

    public void calcPercentage() {
        this.Trainees(6000,8000);
        int totalMarks = marksSecured;
        double OverallPercentage = ((double) totalMarks /(double) totalMaximumMarks)*100;
        System.out.println("total aggregate percentage secured is" +" "+ OverallPercentage);


    }

    public static void main(String[] args) {

        DataProvider obj = new Trainee();
        obj.calcPercentage();

    }
}