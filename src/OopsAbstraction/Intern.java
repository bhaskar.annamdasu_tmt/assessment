package OopsAbstraction;

import java.util.Scanner;

public class Intern implements DataProvider {
    int marksSecured;
    int graceMarks;
    int totalMaximumMarks;


     void Interns(int marksSecured,int graceMarks,int totalMaximumMarks) {
        this.marksSecured = marksSecured;
        this.graceMarks =graceMarks;
        this.totalMaximumMarks=totalMaximumMarks;

    }

    public void calcPercentage() {
         this.Interns(5000,500,8000);
        int totalMarks = marksSecured + graceMarks;
        double OverallPercentage = ((double) totalMarks / (double) totalMaximumMarks)*100;
        System.out.println("total aggregate percentage secured is"+" " + OverallPercentage);

    }

    public static void main(String[] args) {

        DataProvider obj = new Intern();
        obj.calcPercentage();

    }
}
