package Oops;

public class Student extends Person  {
    int []testScore;
    Student(String firstName,String lastName,int idNumber,int[ ]testScore)
    {
        super(firstName,lastName,idNumber);
        this.testScore=testScore;

   }
  char calculate()
  {
      int tally=0;
      for(int score:this.testScore)
      {
          tally=tally+score;
      }
     int average=(tally/this.testScore.length);
      if(average>40)
      {
          if(average>=90)
          {
              return 'o';
          }
          if(average>=80 && average<90)
          {
              return 'p';
          }
          if(average>=70&&average<80)
          {
              return 'q';
          }
      }


   }
}
