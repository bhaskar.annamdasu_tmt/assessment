package Oops;

public class Person {
    String firstName;
    String lastName;
    int idNumber;

    Person(String firstName, String lastName, int idNumber)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.idNumber = idNumber;
    }
    void printPerson(){
        System.out.println("Name:"+lastName+","+firstName+"\nId:"+idNumber);

    }
}