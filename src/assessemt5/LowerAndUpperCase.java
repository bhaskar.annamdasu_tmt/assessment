package assessemt5;

import java.util.Scanner;

public class LowerAndUpperCase {
    public static void main(String[]args){
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the Name");
        String name=sc.next();
        String lower=name.toLowerCase();
        String upper=name.toUpperCase();
        System.out.println("lowercase letters="+lower);
        System.out.println("Uppercase letters="+upper);
    }
}
